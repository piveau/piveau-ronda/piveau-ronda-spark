# Spark

## Functionality
The datastore can process real-time streaming data as well as static datasets.
It uses a Vertx Cluster to run Connector Verticles to fetch real-time data sources and a Webserver Verticle for users to upload datasets.
The incoming real-time data gets forwarded through Apache Kafka, proccessed with Spark Streaming and saved to an HDFS Cluster.
The static datasets can be uploaded to HDFS directly. An Http and Websocket Server is running also in Vertx to provide the processed data.


## Prerequisites
* Docker (Production & Development)
* Docker-compose (Production & Development)
* Maven (Development)
* Java (Development)

## Build Instructions
To build and start the project execute the following command in a shell from the project folder.

1. Start Proprietary Projects by executing: "docker-compose up"
2. Build the project by executing: "mvn package"
3. Start the Spark app by executing: "mvn exec:java"

Under http://localhost:4040 you find the current Spark APP frontend.


## Usage

In the **Spark** project the functionality of the **Normalizer** and **Persister** is implemented.
The **Normalizer** is used to parse the raw incoming data from *Inbound* Kafka Topics into an Open Data format and write
it to *OpenData* Kafka Topics. The **Persister** writes Kafka Messages periodically to HDFS.
The *MainApplication* is used to start *Normalizer* and *Persister* in one Application.
Inside of the streaming Folder, *MainNormalizer* and *MainPersist* are application which can start *normalizer* and *perister* separatly.
Persister and Normalizer need to be registered at in this classes.
The main method of the *MainPerist* and *MainNormalizer* classes serve to start the application.

####Normalizer
In it the spark session is created and the start method is executed.
```java
public class MainNormalizer extends BaseApplication {
    public static Logger LOGGER = Logger.getLogger(MainApplication.class);

    public static void main(String[] args) throws Exception {
        BaseApplication app = new MainNormalizer();
        SparkSession spark = app.createSession(MainNormalizer.class.getName());
        app.start(spark);
        spark.streams().awaitAnyTermination();
        spark.stop();
    }

    @Override
    public void start(SparkSession spark) {
    }
}
```
The *MainNormalizer* class executes all normalizers by calling their start method. In the start method of the *MainNormalizer*
all specific Normalizers can be registered as follows.


```java
public class MainNormalizer extends BaseApplication {
    public static Logger LOGGER = Logger.getLogger(MainNormalizer.class);

    public static void main(String[] args) throws Exception {
        BaseApplication app = new MainNormalizer();
        SparkSession spark = app.createSession(MainNormalizer.class.getName());
        app.start(spark);
        spark.streams().awaitAnyTermination();
        spark.stop();
    }

    @Override
    public void start(SparkSession spark) {
        LOGGER.info("Starting UrbanInstituteNormalizer");
        new UrbanInstituteNormalizer().start(spark);
        LOGGER.info("Starting OpenWeatherMapNormalizer");
        new OpenWeatherMapNormalizer().start(spark);
        LOGGER.info("Starting TestApiNormalizer");
        new TestApiNormalizer().start(spark);
        LOGGER.info("Starting DiscovergyNormalizer");
        new DiscovergyNormalizer().start(spark);
    }
}
```
To create for example *TestAPINormalizer*, we can create a new folder inside of the normalizer folder named after the
data source we want to normalize.
To split it up even further we create a folder named *mapper* inside of this folder.
In there the transformation of data can take place.
A class named *TestApiMapper* is created. This class uses a static *MapFunction*, which is part of  Spark structured Streaming
and an execute function to do the mapping. This abstraction can help when various transformations like: Filter, Mapping and Aggregations need to be applied.
Below is the example of the TestAPIMapper. It maps the TestApi structure definied in one of the previous chapters on itself.
First- and Lastname are exchanged with the * character,except for the first letter. All other values are simply transferred.
The execute function just executes the MapFunction in this example. In more complex examples it can execute various functions in a set order.

```java
public class TestApiMapper {
    static MapFunction<TestApi, TestApi> mapper = testApi -> {
        TestApi result =  new TestApi();
        result.body = testApi.body;
        result.id = testApi.id;
        result.timestamp = testApi.timestamp;
        result.title = testApi.title;
        result.userId = testApi.userId;

        char[] firstname = testApi.firstname.trim().toCharArray();
        for(int i=0; i<firstname.length; i++){
            if(i>0){
                firstname[i] = '*';
            }
        }
        result.firstname = String.valueOf(firstname);

        char[] lastname = testApi.lastname.trim().toCharArray();
        for(int i=0; i<lastname.length; i++){
            if(i>0){
                lastname[i] = '*';
            }
        }
        result.lastname = String.valueOf(lastname);

        return result;
    };

    public static Dataset<TestApi> execute(Dataset<TestApi> dataset){
        return dataset.map(mapper, Encoders.bean(TestApi.class));
    }
}
```
The actual Normalizer class inherits the *BaseApplication* class and therefore need to implement the *start* method.
In this method the *getDatasetFromKafka* method is called. The parameters are the *SparkContext* and the Topic and Key definied in an earlier chapter.
The result of this method is an already typed Dataset based on the structure the KafkaKey is holding.
On the resulting dataset, the previously definied *TestApiMapper* is executed.
The resulting dataset is written to Kafka on the *OpenData.Test* definied in the previous chapter.
```java
public class TestApiNormalizer extends BaseApplication {
    @Override
    public void start(SparkSession spark) {
        Dataset<TestApi> dataset = getDatasetFromKafka(spark,
                KafkaTopics.INBOUND_TESTAPI, KafkaKeys.INBOUND_TESTAPI_KEY);

        Dataset<TestApi> anonymizedDataset = TestApiMapper.execute(dataset);

        writeDatasetToKafka(anonymizedDataset, KafkaTopics.OPENDATA_TEST,
                KafkaKeys.INBOUND_TESTAPI_KEY, "TestapiToOpenData");
    }
}
```
####Persister
The persister is created in a similar way as normalizers.
In the *mainPersist* class the *StreamsToOpenData* Persister is regsitered.
````java
/**
 * The MainPersister start all Persister applications registered in its start mehtod.
 */
public class MainPersist extends BaseApplication {
    public static Logger LOGGER =  Logger.getLogger(MainPersist.class);

    public static void main(String[] args) throws Exception {
        BaseApplication app = new MainPersist();
        SparkSession spark = app.createSession(MainPersist.class.getName());
        app.start(spark);
        spark.streams().awaitAnyTermination();
        spark.stop();
    }
    
    @Override
    public void start(SparkSession spark) {
        LOGGER.info("Starting MainPerister");
        new StreamsToOpenDataApp().start(spark);
    }
}
````
Afterwards a class which inherits *BaseApplication* is created for the *StreamsToOpenDataApp* Persister.
In this classes *start* method the actual logic is implemented.


````java
public class StreamsToOpenDataApp extends BaseApplication {
    private static final Logger LOGGER = Logger.getLogger(StreamsToOpenDataApp.class.getName());
    private static final Long BatchWindowInMinutes = Long.parseLong(
            properties.getProperty("io.piveau.DataStore.persistWindowMinutes", "60")
    );
    public static Set<Topic> advertisedTopics = KafkaTopics.openDataTopics();

    @Override
    public void start(SparkSession spark) {
        this.spark = spark;
        for(Topic topic: advertisedTopics){
                Key<?> key = topic.keys.iterator().next();
                LOGGER.info("Starting Persister for: " + topic.topicName);
                Dataset<?> dataset = this.getDatasetFromKafka(spark, topic, key);
                this.persistAsCSV(dataset, topic, key);
                this.persistAsJson(dataset, topic, key);
        }
    }

//Further functionality ...

}
```` 
In the example above for all OpenData Topics the method *persistAsCSV* and *persistAsJson* is called.
The functionality of these methods are displayed below.
````java

public class StreamsToOpenDataApp extends BaseApplication {
    private static final Logger LOGGER = Logger.getLogger(StreamsToOpenDataApp.class.getName());
    private static final Long BatchWindowInMinutes = Long.parseLong(
            properties.getProperty("io.piveau.DataStore.persistWindowMinutes", "60")
    );
    public static Set<Topic> advertisedTopics = KafkaTopics.openDataTopics();
   
    public void persistAsJson(Dataset<?> dataset, Topic topic, Key<?> key){
        if(!topic.keys.contains(key))
            throw new IllegalArgumentException("Key is not registered with topic");

        dataset
                .withColumn("year", functions.year(functions.column("timestamp")))
                .withColumn("month",functions.month(functions.column("timestamp")))
                .withColumn("day", functions.dayofmonth(functions.column("timestamp")))
                .withWatermark("timestamp", BatchWindowInMinutes + " minutes")
                .writeStream()
                .queryName("BatchToJson-" + topic.topicName + "-"+ key.keyName)
                .trigger(Trigger.ProcessingTime(60000 * BatchWindowInMinutes))
                .outputMode(OutputMode.Append())
                .format("json") // Output Format
                .option("path", hdfsStructure.topicDirectory(topic, key, DataFormat.JSON))
                .option("checkpointLocation", hdfsStructure.checkpointDirectory(appName, "BatchToJson-" + topic.topicName + "-"+ key.keyName))
                .partitionBy("year", "month", "day") // Folder Structure
                .start();
    }
    
    public void persistAsCSV(Dataset<?> dataset, Topic topic, Key<?> key){
            if(!topic.keys.contains(key))
                throw new IllegalArgumentException("Key is not registered with topic");
    
            Dataset<?> flattenDataset = flattenDataset(dataset.as(Encoders.bean(key.schema))
                    .withColumn("year", functions.year(functions.column("timestamp")))
                    .withColumn("month",functions.month(functions.column("timestamp")))
                    .withColumn("day", functions.dayofmonth(functions.column("timestamp"))));
    
            flattenDataset
                    .withWatermark("timestamp", 2 * BatchWindowInMinutes + " minutes")
                    .writeStream()
                    .queryName("BatchToCSV-" + topic.topicName + key.keyName)
                    .trigger(Trigger.ProcessingTime(60000 * BatchWindowInMinutes))
                    .outputMode(OutputMode.Append())
                    .format("csv") // Output Format
                    .option("header", "true")
                    .option("path", hdfsStructure.topicDirectory(topic,key, DataFormat.CSV))
                    .option("checkpointLocation", hdfsStructure.checkpointDirectory(appName, "BatchToCSV-" + topic.topicName + key.keyName))
                    .partitionBy("year", "month", "day") // Folder Structure
                    .start();
        }


}
````
For both CSV and JSON persisting, Year, month and day are declared as seperate columns with the parsed value of the timestamp and afterwards the data is partitioned by those columns.
This results in a folder structure on hdfs where data is in seperated folders for each year, month and day.

A  Trigger, Watermark and queryName is set. With the hdfsStructure class declared in the BaseApplication the location for the data and checkpoint is set.
For CSV the data needs to be flattened additionally as no nested columns are allowed. The functionality for this is also declared in the *BaseApplication* and can simply be used by calling the *flattenDataset* method.

The trigger determines the frequency for the persisting process. The end user can download the data imidiatly after the persisting process has finished.
In case of the default 60 mins. The newest data can be downloaded in batches every 60 minutes.