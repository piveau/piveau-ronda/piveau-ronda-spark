FROM bde2020/spark-submit:2.4.0-hadoop2.8-scala2.12

ENV LOCAL_SPARK_APP_LOCATION target/Spark-1.0-fat.jar
ENV SPARK_APPLICATION_MAIN_CLASS io.piveau.DataStore.Spark.applications.MainApplication
ENV SPARK_SUBMIT_ARGS --packages org.apache.spark:spark-sql-kafka-0-10_2.12:2.4.0 --driver-memory 500M
ENV ENABLE_INIT_DAEMON false


ENV SPARK_APPLICATION_JAR_LOCATION /usr/src/app/Spark-App.jar
COPY $LOCAL_SPARK_APP_LOCATION $SPARK_APPLICATION_JAR_LOCATION
