package io.piveau.DataStore.Spark.applications.streaming.normalizer;

import io.piveau.DataStore.Spark.applications.MainApplication;
import io.piveau.DataStore.Spark.applications.base.BaseApplication;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.discovergy.DiscovergyNormalizer;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.openWeatherMap.OpenWeatherMapNormalizer;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.testApi.TestApiNormalizer;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.urbanInstitute.UrbanInstituteNormalizer;
import org.apache.log4j.Logger;
import org.apache.spark.sql.SparkSession;

/**
 * The MainNormalizer starts all Normalizer Applications registered in its start message
 */
public class MainNormalizer extends BaseApplication {
    public static Logger LOGGER = Logger.getLogger(MainApplication.class);

    public static void main(String[] args) throws Exception {
        BaseApplication app = new MainNormalizer();
        SparkSession spark = app.createSession(MainNormalizer.class.getName());
        app.start(spark);
        spark.streams().awaitAnyTermination();
        spark.stop();
    }

    @Override
    public void start(SparkSession spark) {
        LOGGER.info("Starting UrbanInstituteNormalizer");
        new UrbanInstituteNormalizer().start(spark);
        LOGGER.info("Starting OpenWeatherMapNormalizer");
        new OpenWeatherMapNormalizer().start(spark);
        LOGGER.info("Starting TestApiNormalizer");
        new TestApiNormalizer().start(spark);
        LOGGER.info("Starting DiscovergyNormalizer");
        new DiscovergyNormalizer().start(spark);


    }
}

