package io.piveau.DataStore.Spark.applications.streaming.persist;

import io.piveau.DataStore.Commons.schema.DataFormat;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Commons.schema.base.Key;
import io.piveau.DataStore.Commons.schema.base.Topic;
import io.piveau.DataStore.Spark.applications.base.BaseApplication;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.Trigger;

import java.util.*;
import java.util.logging.Logger;

/**
 * The StreamsToOpenDataApp periodically saves all Open Data streams from Kafka to HDFS in JSON as well as CSV format.
 */
public class StreamsToOpenDataApp extends BaseApplication {
    private static final Logger LOGGER = Logger.getLogger(StreamsToOpenDataApp.class.getName());
    private static final Long BatchWindowInMinutes = Long.parseLong(
            properties.getProperty("io.piveau.DataStore.persistWindowMinutes", "60")
    );
    public static Set<Topic> advertisedTopics = KafkaTopics.openDataTopics();

    @Override
    public void start(SparkSession spark) {
        this.spark = spark;
        for(Topic topic: advertisedTopics){
                Key<?> key = topic.keys.iterator().next();
                LOGGER.info("Starting Persister for: " + topic.topicName);
                Dataset<?> dataset = this.getDatasetFromKafka(spark, topic, key);
                this.persistAsCSV(dataset, topic, key);
                this.persistAsJson(dataset, topic, key);
        }
    }

    public void persistAsJson(Dataset<?> dataset, Topic topic, Key<?> key){
        if(!topic.keys.contains(key))
            throw new IllegalArgumentException("Key is not registered with topic");

        dataset
                .withColumn("year", functions.year(functions.column("timestamp")))
                .withColumn("month",functions.month(functions.column("timestamp")))
                .withColumn("day", functions.dayofmonth(functions.column("timestamp")))
                .withWatermark("timestamp", BatchWindowInMinutes + " minutes")
                .writeStream()
                .queryName("BatchToJson-" + topic.topicName + "-"+ key.keyName)
                .trigger(Trigger.ProcessingTime(60000 * BatchWindowInMinutes))
                .outputMode(OutputMode.Append())
                .format("json") // Output Format
                .option("path", hdfsStructure.topicDirectory(topic, key, DataFormat.JSON))
                .option("checkpointLocation", hdfsStructure.checkpointDirectory(appName, "BatchToJson-" + topic.topicName + "-"+ key.keyName))
                .partitionBy("year", "month", "day") // Folder Structure
                .start();
    }
    public void persistAsCSV(Dataset<?> dataset, Topic topic, Key<?> key){
        if(!topic.keys.contains(key))
            throw new IllegalArgumentException("Key is not registered with topic");

        Dataset<?> flattenDataset = flattenDataset(dataset.withWatermark("timestamp", 2 * BatchWindowInMinutes + " minutes").as(Encoders.bean(key.schema))
                .withColumn("year", functions.year(functions.column("timestamp")))
                .withColumn("month",functions.month(functions.column("timestamp")))
                .withColumn("day", functions.dayofmonth(functions.column("timestamp"))));

        flattenDataset
                .withWatermark("timestamp", 2 * BatchWindowInMinutes + " minutes")
                .writeStream()
                .queryName("BatchToCSV-" + topic.topicName + key.keyName)
                .trigger(Trigger.ProcessingTime(60000 * BatchWindowInMinutes))
                .outputMode(OutputMode.Append())
                .format("csv") // Output Format
                .option("header", "true")
                .option("path", hdfsStructure.topicDirectory(topic,key, DataFormat.CSV))
                .option("checkpointLocation", hdfsStructure.checkpointDirectory(appName, "BatchToCSV-" + topic.topicName + key.keyName))
                .partitionBy("year", "month", "day") // Folder Structure
                .start();
    }
}
