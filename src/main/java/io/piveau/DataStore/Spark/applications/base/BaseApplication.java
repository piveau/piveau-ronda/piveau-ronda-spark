package io.piveau.DataStore.Spark.applications.base;

import io.piveau.DataStore.Commons.schema.HdfsStructure;
import io.piveau.DataStore.Commons.schema.base.Key;
import io.piveau.DataStore.Commons.schema.base.Topic;
import io.piveau.DataStore.Spark.util.PropertyHelper;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.DataStreamWriter;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.Trigger;
import org.apache.spark.sql.types.ArrayType;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.collection.JavaConverters;
import scala.collection.Seq;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The BaseApplications provides functionalities for reading and writing to Kafka and creating Saprk Sessions for
 * Spark Applications
 */
public abstract class BaseApplication {
    protected static Properties properties = properties();
    protected static SparkConf sparkConf = sparkConf();
    protected static HdfsStructure hdfsStructure = new HdfsStructure(sparkConf().get("hdfs.rootDir"));
    public String appName = this.getClass().getName();
    protected Map<String, String> kafkaConf = kafkaConf();
    protected SparkSession spark;

    // Config Methods
    public static Properties properties() {
        Properties result = new Properties();
        try {
            result = PropertyHelper.getProperties();
        } catch (IOException e) {
            System.out.println("Could not load properties");
            e.printStackTrace();
        }
        return result;
    }

    public static SparkConf sparkConf() {
        Properties prop = new Properties();
        try {
            prop = PropertyHelper.getProperties();
        } catch (IOException e) {
            System.out.println("Could not load properties");
            e.printStackTrace();
        }
        String driver_host = System.getenv("SPARK_DRIVER_HOST");
        String spark_master = System.getenv("SPARK_MASTER");
        return new SparkConf()
                // Spark Config
                .set("spark.master", (spark_master != null) ? spark_master: prop.getProperty("io.piveau.DataStore.spark.master"))
                .set("spark.driver.bindAddress", "0.0.0.0")
                .set("spark.cores.max", prop.getProperty("io.piveau.DataStore.spark.cores.max"))
                .set("spark.driver.host", driver_host != null ? driver_host : prop.getProperty("io.piveau.DataStore.spark.driver.host"))
                .set("spark.driver.port", prop.getProperty("io.piveau.DataStore.spark.driver.port"))
                .set("spark.ui.proxyBase", prop.getProperty("io.piveau.DataStore.spark.ui.proxyBase"))
                .set("spark.cleaner.referenceTracking.blocking", "true")
                .set("spark.cleaner.referenceTracking.blocking.shuffle", "true")
                .set("spark.cleaner.referenceTracking.cleanCheckpoints", "true")
                .set("spark.hadoop.parquet.enable.summary-metadata", "false") // Disable Spark Meta folder
                // 4 executor per instance of each worker
                .set("spark.executor.instances", "1")
                // 5 cores on each executor
                .set("spark.executor.cores", "2")
                .set("spark.executor.memory", "1g")
                .set("spark.scheduler.mode", "FAIR")
                .set("spark.network.timeout", "50000")
                .set("spark.rpc.lookupTimeout","1800")
                .set("spark.scheduler.blacklist.unschedulableTaskSetTimeout","1800")
                .set("spark.sql.bigdata.streaming.checkPointFileslimit", "100")
                .set("spark.checkpoint.compress", "true")
                .set("spark.cleaner.periodicGC.interval", "10min")
                // Kafka Config
                .set("kafka.bootstrap.servers", prop.getProperty("io.piveau.DataStore.kafka.brokerlist"))
                .set("kakfa.metadata.max.age.ms", prop.getProperty("io.piveau.DataStore.kafka.metadata.max.age.ms"))
                //HDFS Config
                .set("spark.hadoop.dfs.client.use.datanode.hostname", "true")
                .set("hdfs.rootDir", prop.getProperty("io.piveau.DataStore.hdfs"));

    }

    public abstract void start(SparkSession spark);

    public Map<String, String> kafkaConf() {
        Map<String, String> kafkaParams = new HashMap<>();
        kafkaParams.put("format", "kafka");
        kafkaParams.put("kafka.bootstrap.servers", sparkConf.get("kafka.bootstrap.servers"));
        kafkaParams.put("kafka.metadata.max.age.ms", sparkConf.get("kakfa.metadata.max.age.ms"));
        kafkaParams.put("enable.partition.eof", "false");
        kafkaParams.put("failOnDataLoss", "false");
        kafkaParams.put("auto.offset.reset", "earliest");
        kafkaParams.put("kafka.partitioner.class", "io.piveau.DataStore.Commons.schema.DataStorePartitioner");
        kafkaParams.put("kafka.request.timeout.ms", "100000");
        return kafkaParams;
    }

    // Helper Methods
    public SparkSession createSession(String appName) {
        SparkSession session = SparkSession.builder().appName(appName).config(sparkConf).getOrCreate();
        this.appName = appName;
        session.sparkContext().setLogLevel("ERROR");

        return session;
    }

    public <T> Dataset<T> getDatasetFromKafka(SparkSession spark, Topic topic, Key<T> key) {
        Encoder<T> schemaEncoder = Encoders.bean(key.schema);
        if (!topic.keys.contains(key))
            throw new IllegalArgumentException("Key is not registered with Topic");

        Dataset<Row> rawMessage =
                spark
                        .readStream()
                        .format("kafka")
                        .options(kafkaConf)
                        .option("subscribe", topic.topicName)
                        .load();

        return rawMessage
                .filter(functions.col("key").cast("string").rlike(key.keyName))
                .select(
                        org.apache.spark.sql.functions.from_json(
                                functions.column("value").cast("string"),
                                schemaEncoder.schema(),
                                key.sparkOptions
                        ).as("value"))
                .select(functions.column("value.*"))
                .as(schemaEncoder);

    }

    public static Dataset<?> flattenDataset(Dataset<?> ds) {

        StructField[] fields = ds.schema().fields();

        List<String> fieldsNames = new ArrayList<>();
        for (StructField s : fields) {
            fieldsNames.add(s.name());
        }

        for (StructField field : fields) {

            DataType fieldType = field.dataType();
            String fieldName = field.name();

            if (fieldType instanceof ArrayType) {
                List<String> fieldNamesExcludingArray = new ArrayList<String>();
                for (String fieldName_index : fieldsNames) {
                    if (!fieldName.equals(fieldName_index))
                        fieldNamesExcludingArray.add(fieldName_index);
                }

                List<String> fieldNamesAndExplode = new ArrayList<>(fieldNamesExcludingArray);
                String s = String.format("explode_outer(%s) as %s", fieldName, fieldName);
                fieldNamesAndExplode.add(s);

                String[] exFieldsWithArray = new String[fieldNamesAndExplode.size()];
                Dataset<?> exploded_ds = ds.selectExpr(fieldNamesAndExplode.toArray(exFieldsWithArray));

                return flattenDataset(exploded_ds);

            } else if (fieldType instanceof StructType) {

                String[] childFieldnames_struct = ((StructType) fieldType).fieldNames();

                List<String> childFieldnames = new ArrayList<>();
                for (String childName : childFieldnames_struct) {
                    childFieldnames.add(fieldName + "." + childName);
                }

                List<String> newfieldNames = new ArrayList<>();
                for (String fieldName_index : fieldsNames) {
                    if (!fieldName.equals(fieldName_index))
                        newfieldNames.add(fieldName_index);
                }

                newfieldNames.addAll(childFieldnames);

                List<Column> renamedStrutctCols = new ArrayList<>();

                for (String newFieldNames_index : newfieldNames) {
                    renamedStrutctCols.add(new Column(newFieldNames_index.toString()).as(newFieldNames_index.toString().replace(".", "_")));
                }

                Seq<Column> renamedStructCols_seq = JavaConverters.collectionAsScalaIterableConverter(renamedStrutctCols).asScala().toSeq();

                Dataset<?> ds_struct = ds.select(renamedStructCols_seq);

                return flattenDataset(ds_struct);
            }
        }
        return ds;
    }

    public Dataset<Row> getRowDatasetFromKafka(SparkSession spark, Topic topic) {
        return spark
                .readStream()
                .format("kafka")
                .options(kafkaConf)
                .option("subscribe", topic.topicName)
                .load();
    }

    public <T> Dataset<T> filterKey(Dataset<Row> kafkaMessage, Key<T> key, Encoder<T> schemaEncoder){
        return kafkaMessage
                .filter(functions.col("key").cast("string").rlike(key.keyName))
                .select(
                        org.apache.spark.sql.functions.from_json(
                                functions.column("value").cast("string"),
                                schemaEncoder.schema(),
                                key.sparkOptions
                        ).as("value"))
                .select(functions.column("value.*"))
                .as(schemaEncoder);
    }
    public <T> Dataset<T> filterTopic(Dataset<Row> kafkaMessage, Topic topic, Key<T> key){
        Encoder<T> schemaEncoder = Encoders.bean(key.schema);

        return kafkaMessage.filter(functions.col("topic").like(topic.topicName))
                .filter(functions.col("key").cast("string").rlike(key.keyName))
                .select(
                        org.apache.spark.sql.functions.from_json(
                                functions.column("value").cast("string"),
                                schemaEncoder.schema(),
                                key.sparkOptions
                        ).as("value"))
                .select(functions.column("value.*"))
                .as(schemaEncoder);
    }

    public  Map<Key<?>, Dataset<?>> filterTopic(Dataset<Row> kafkaMessage, Topic topic){
        return topic.keys.stream()
                .collect(Collectors.toMap(key -> key,
                        key -> filterTopic(kafkaMessage, topic, key)));
    }

    public Map<Topic, Map<Key<?>, Dataset<?>>> filterTopics(Dataset<Row> kafkaMessage, List<Topic> topics){
        return topics.stream()
                .collect(Collectors.toMap(topic -> topic,
                        topic -> filterTopic(kafkaMessage, topic)));
    }

    public void logDataset(Dataset<?> dataset){
            dataset.writeStream()
                    .outputMode(OutputMode.Append())
                    .option("checkpointLocation", hdfsStructure.checkpointDirectory(appName,"logger-" + UUID.randomUUID().toString()))
                    .format("console")
                    .start();
    }

    public <T> void writeDatasetToKafka(Dataset<T> dataset, Topic topic, Key<T> key, String queryName) {
        writeDatasetToKafka(dataset, topic, key, queryName, Trigger.ProcessingTime(5000));
    }
    public <T> void writeDatasetToKafka(Dataset<T> dataset, Topic topic, Key<T> key, String queryName, Trigger trigger) {
        if (!topic.keys.contains(key))
            throw new IllegalArgumentException("Schema Key is not registered with Topic");

        Dataset<Row> output = dataset
                .select(functions.to_json(functions.struct("*")).alias("value"))
                .withColumn("key", functions.lit(key.keyName));

        // Write to Kafka Stream
        DataStreamWriter<Row> writer = output.writeStream();

        if(trigger != null)
           writer.trigger(trigger);

        writer
                .queryName(this.appName + ":" + queryName)
                .format("kafka")
                .outputMode(OutputMode.Append())
                .options(kafkaConf)
                .option("checkpointLocation", hdfsStructure.checkpointDirectory(this.appName, queryName))
                .option("topic", topic.topicName)
                .start();
    }
}
