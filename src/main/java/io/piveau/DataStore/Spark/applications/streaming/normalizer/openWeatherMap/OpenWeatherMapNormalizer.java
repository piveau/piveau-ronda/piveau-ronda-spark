package io.piveau.DataStore.Spark.applications.streaming.normalizer.openWeatherMap;

import io.piveau.DataStore.Commons.schema.KafkaKeys;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Commons.schema.harvester.inbound.openWeatherMap.OpenWeatherMap;
import io.piveau.DataStore.Commons.schema.harvester.openData.openWeather.CityWeather;
import io.piveau.DataStore.Spark.applications.base.BaseApplication;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.openWeatherMap.mapper.OpenWeatherMapToCityWeather;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * The Open WeatherMap Normalizer maps data from the INBOUND_OPENWEATHERMAP key to the OPENDATA_CITYWEATHER key
 * and saves it to the OPENDATA_OPENWEATHER_CITY topic
 */
public class OpenWeatherMapNormalizer extends BaseApplication {

    @Override
    public void start(SparkSession spark) {
        Dataset<OpenWeatherMap> openWeatherMapDataset = getDatasetFromKafka(spark,
                KafkaTopics.INBOUND_OPENWEATHERMAP,
                KafkaKeys.INBOUND_OPENWEATHERMAP);

        openWeatherMapDataset.withWatermark("timestamp", "2 hours");
        Dataset<CityWeather> cityWeatherDataset = OpenWeatherMapToCityWeather.execute(openWeatherMapDataset);

        writeDatasetToKafka(cityWeatherDataset,
                KafkaTopics.OPENDATA_OPENWEATHER_CITY,
                KafkaKeys.OPENDATA_CITYWEATHER,
                "OpenWeatherMapToCityWeather"
        );

    }
}
