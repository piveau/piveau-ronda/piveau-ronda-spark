package io.piveau.DataStore.Spark.applications.streaming.normalizer.urbanInstitute;

import io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute.*;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.electricity.Electricity;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.electricity.ElectricityBatch;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.gas.Gas;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.gas.GasBatch;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.water.Water;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.water.WaterBatch;
import io.piveau.DataStore.Spark.applications.base.BaseApplication;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.urbanInstitute.mapper.DiscovergyToSmartEnergy;
import io.piveau.DataStore.Commons.schema.KafkaKeys;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.streaming.Trigger;

public class UrbanInstituteNormalizer extends BaseApplication {
    public static Logger LOGGER = Logger.getLogger(UrbanInstituteNormalizer.class);

    public void start(SparkSession spark) {
        handleOpenWeather(spark);

        Dataset<DiscovergyEvent> discovergyEvent = getDatasetFromKafka(spark,
                KafkaTopics.INBOUND_URBANINSTITUTE,
                KafkaKeys.INBOUND_URBANINSTITUTE_DISCOVERGY);



        Dataset<Electricity> elec =
                DiscovergyToSmartEnergy.toElectricity(discovergyEvent, "1 hour", "10 minute");
        Dataset<Water> water =
                DiscovergyToSmartEnergy.toWater(discovergyEvent, "1 hour", "10 minutes");
        Dataset<Gas> gas =
                DiscovergyToSmartEnergy.toGas(discovergyEvent, "1 hour", "10  minutes");


        //writeDatasetToKafka(elec,KafkaTopics.OPENDATA_SMARTENERGY_ELECTRICITY, KafkaKeys.OPENDATA_SMARTENERGY_ELECTRICITY, "ELECTRICITY_OPEN",Trigger.ProcessingTime(60 * 60000));
        //writeDatasetToKafka(gas,KafkaTopics.OPENDATA_SMARTENERGY_GAS, KafkaKeys.OPENDATA_SMARTENERGY_GAS, "GAS_OPEN", Trigger.ProcessingTime(60 * 60000));
        //writeDatasetToKafka(water,KafkaTopics.OPENDATA_SMARTENERGY_Water, KafkaKeys.OPENDATA_SMARTENERGY_WATER, "WATER_OPEN",Trigger.ProcessingTime(60 * 60000));

        writeDatasetToKafka(elec,KafkaTopics.INBOUND_URBANINSTITUTE_BATCH_SIMPLE, KafkaKeys.OPENDATA_SMARTENERGY_ELECTRICITY, "ELECTRICITY_IN",Trigger.ProcessingTime(60 * 60000));
        writeDatasetToKafka(gas,KafkaTopics.INBOUND_URBANINSTITUTE_BATCH_SIMPLE, KafkaKeys.OPENDATA_SMARTENERGY_GAS, "GAS_IN", Trigger.ProcessingTime(60 * 60000));
        writeDatasetToKafka(water,KafkaTopics.INBOUND_URBANINSTITUTE_BATCH_SIMPLE, KafkaKeys.OPENDATA_SMARTENERGY_WATER, "WATER_IN",Trigger.ProcessingTime(60 * 60000));

        Dataset<Electricity> elec_simple = getDatasetFromKafka(spark, KafkaTopics.INBOUND_URBANINSTITUTE_BATCH_SIMPLE, KafkaKeys.OPENDATA_SMARTENERGY_ELECTRICITY);
        Dataset<Gas> gas_simple = getDatasetFromKafka(spark, KafkaTopics.INBOUND_URBANINSTITUTE_BATCH_SIMPLE, KafkaKeys.OPENDATA_SMARTENERGY_GAS);
        Dataset<Water> water_simple = getDatasetFromKafka(spark, KafkaTopics.INBOUND_URBANINSTITUTE_BATCH_SIMPLE, KafkaKeys.OPENDATA_SMARTENERGY_WATER);

        Dataset<ElectricityBatch> elec_imternediate = DiscovergyToSmartEnergy.toElectricityBatch(elec_simple, "1 day", "2 hours");
        Dataset<GasBatch> gas_imternediate = DiscovergyToSmartEnergy.toGasBatch(gas_simple, "1 day", "2 hours");
        Dataset<WaterBatch> water_imternediate = DiscovergyToSmartEnergy.toWaterBatch(water_simple, "1 day", "2 hours");

        writeDatasetToKafka(elec_imternediate,KafkaTopics.INBOUND_URBANINSTITUTE_BATCH_INTERMEDIATE, KafkaKeys.INBOUND_URBANINSTITUTE_DISCOVERGY_ELEC_BATCH, "ELECTRICITY_BATCH",Trigger.ProcessingTime(60  * 60000));
        writeDatasetToKafka(gas_imternediate,KafkaTopics.INBOUND_URBANINSTITUTE_BATCH_INTERMEDIATE, KafkaKeys.INBOUND_URBANINSTITUTE_DISCOVERGY_GAS_BATCH, "GAS_BATCH", Trigger.ProcessingTime(60  * 60000));
        writeDatasetToKafka(water_imternediate,KafkaTopics.INBOUND_URBANINSTITUTE_BATCH_INTERMEDIATE, KafkaKeys.INBOUND_URBANINSTITUTE_DISCOVERGY_WATER_BATCH, "WATER_BATCH",Trigger.ProcessingTime(60  * 60000));




    }

    private void handleOpenWeather(SparkSession spark){
        Dataset<OpenWeatherMapEvent> message = getDatasetFromKafka(spark,KafkaTopics.INBOUND_URBANINSTITUTE, KafkaKeys.INBOUND_URBANINSTITUTE_OPENWEATHER);
        writeDatasetToKafka(message, KafkaTopics.OPENDATA_OPENWEATHER, KafkaKeys.INBOUND_URBANINSTITUTE_OPENWEATHER, "OPENWEATHER", Trigger.ProcessingTime(3 * 1000));
    }

}
