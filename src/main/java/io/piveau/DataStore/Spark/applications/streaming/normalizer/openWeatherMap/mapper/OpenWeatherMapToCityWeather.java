package io.piveau.DataStore.Spark.applications.streaming.normalizer.openWeatherMap.mapper;

import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import io.piveau.DataStore.Commons.schema.harvester.inbound.openWeatherMap.OpenWeatherMap;
import io.piveau.DataStore.Commons.schema.harvester.openData.openWeather.CityWeather;
import io.piveau.DataStore.Commons.schema.harvester.openData.snippets.Coordinates;
import io.piveau.DataStore.Commons.schema.harvester.openData.snippets.TypedValue;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.MainNormalizer;
import org.apache.commons.collections.iterators.EmptyIterator;
import org.apache.commons.collections.iterators.EmptyListIterator;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class OpenWeatherMapToCityWeather {
    public static Logger LOGGER = Logger.getLogger(OpenWeatherMapToCityWeather.class);
    public static Dataset<CityWeather> execute(Dataset<OpenWeatherMap> dataset){
        return dataset.flatMap(new Mapper(), Encoders.bean(CityWeather.class));
    }

    public static class Mapper implements FlatMapFunction<OpenWeatherMap, CityWeather> {

        private static CityWeather transform(OpenWeatherMap openWeatherMap){
            CityWeather result = new CityWeather();
            result.setTimestamp(openWeatherMap.getTimestamp());

            if(openWeatherMap.getMain() == null || openWeatherMap.getCoord() == null)
                throw new IllegalArgumentException("Input provides not enough data for mapping");

            // Parse Coordinates
            if(openWeatherMap.getCoord().getLat() != null && openWeatherMap.getCoord().getLon() != null) {
                Coordinates coordinates = new Coordinates();
                coordinates.setLat(Float.toString(openWeatherMap.getCoord().getLat()));
                coordinates.setLon(Float.toString(openWeatherMap.getCoord().getLon()));
                result.setCoordinates(coordinates);
            }

            // Parse Humidity
            if(openWeatherMap.getMain().getHumidity() != null) {
                TypedValue<Float> humidty = new TypedValue<>();
                humidty.setValue(openWeatherMap.getMain().getHumidity());
                humidty.setUnit("g/m3");
                result.setHumidity(humidty);
            }

            // Parse Temperature
            if(openWeatherMap.getMain().getTemp() != null) {
                TypedValue<Float> temperature = new TypedValue<>();
                temperature.setValue(openWeatherMap.getMain().getTemp());
                temperature.setUnit("Celcius");
                result.setTemperature(temperature);
            }

            //Air Pressure
            if(openWeatherMap.getMain().getPressure() != null) {
                TypedValue<Float> pressure = new TypedValue<>();
                pressure.setValue(openWeatherMap.getMain().getPressure());
                pressure.setUnit("Pascal");
                result.setPressure(pressure);
            }
            return result;
        }

        @Override
        public Iterator<CityWeather> call(OpenWeatherMap openWeatherMap) throws Exception {
            try {
                CityWeather result = transform(openWeatherMap);
                return Arrays.asList(result).iterator();
            }catch (Exception exception){
                LOGGER.error("Transformation Error");
                return Collections.emptyIterator();
            }

        }
    }
}
