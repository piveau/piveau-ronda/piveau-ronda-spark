package io.piveau.DataStore.Spark.applications.streaming.normalizer.urbanInstitute.mapper;

import io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute.DiscovergyEvent;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.electricity.Electricity;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.electricity.ElectricityBatch;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.gas.Gas;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.gas.GasBatch;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.water.Water;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.water.WaterBatch;
import org.apache.spark.sql.*;
import scala.collection.JavaConverters;

import java.util.*;

public class DiscovergyToSmartEnergy {

    public static Dataset<Water> toWater(Dataset<DiscovergyEvent> dataset, String window, String watermark){
        List<String> columns = new LinkedList<>();
        columns.add("reading.values.volume");
        columns.add("reading.values.volumeFlow");

        Dataset<Row> result = execute(dataset.filter(functions.col("measurementType").rlike("WATER")),columns,window, watermark);
        return result.as(Encoders.bean(Water.class));

    }

    public static Dataset<Electricity> toElectricity(Dataset<DiscovergyEvent> dataset, String window, String watermark){
        List<String> columns = new LinkedList<>();
        columns.add("reading.values.energy");
        columns.add("reading.values.energy1");
        columns.add("reading.values.energy2");
        columns.add("reading.values.energyOut");
        columns.add("reading.values.energyOut1");
        columns.add("reading.values.energyOut2");
        columns.add("reading.values.power");

        Dataset<Row> result = execute(dataset.filter(functions.col("measurementType").rlike("ELECTRICITY")),columns,window, watermark);
        return result.as(Encoders.bean(Electricity.class));

    }

    public static Dataset<Gas> toGas(Dataset<DiscovergyEvent> dataset, String window, String watermark){
        List<String> columns = new LinkedList<>();
        columns.add("reading.values.volume");
        columns.add("reading.values.actualityDuration");


        Dataset<Row> result = execute(dataset.filter(functions.col("measurementType").rlike("GAS")),columns,window, watermark);
        return result.as(Encoders.bean(Gas.class));

    }


    public static Dataset<GasBatch> toGasBatch(Dataset<Gas> dataset, String window, String watermark){
        return toBatch(dataset, window, watermark).as(Encoders.bean(GasBatch.class));
    }

    public static Dataset<WaterBatch> toWaterBatch(Dataset<Water> dataset, String window, String watermark){
        return toBatch(dataset, window, watermark).as(Encoders.bean(WaterBatch.class));
    }
    public static Dataset<ElectricityBatch> toElectricityBatch(Dataset<Electricity> dataset, String window, String watermark){
        return toBatch(dataset, window, watermark).as(Encoders.bean(ElectricityBatch.class));
    }

    private static Dataset<Row> toBatch(Dataset<?> dataset, String window, String watermark){

        return dataset
                .select(functions.col("timestamp"), functions.struct("*").alias("reading"))
                .withWatermark("timestamp", watermark)
                .groupBy(functions.window(functions.column("timestamp"), window))
                .agg(functions.collect_list("reading").alias("values"));
    }

    private static Dataset<Row> execute(Dataset<DiscovergyEvent> dataset, List<String> columns, String window, String watermark){

        Dataset<Row> preparedDataset = dataset.select(functions.col("timestamp"), functions.struct("*").alias("reading"));

        List<Column> expr = new LinkedList<>();
        for(String colName: columns){
            String[] nameSplit = colName.split("\\.");
            String name = nameSplit[nameSplit.length -1];
            expr.add(functions.struct(
                    functions.avg(colName).cast("Double").alias("avg"),
                    functions.min(colName).alias("min"),
                    functions.max(colName).alias("max"),
                    functions.stddev(functions.col(colName).cast("Double")).alias("stddev")

            ).alias(name));
        }
        JavaConverters.asScalaBufferConverter(expr).asScala().toSeq();

        return preparedDataset
                .withWatermark("timestamp", watermark)
                .groupBy(functions.window(functions.column("timestamp"), window))
                .agg(functions.count(functions.lit(1)).alias("count"),
                        JavaConverters.asScalaBufferConverter(expr).asScala().toSeq()
                )
                .select(functions.column("*"), functions.column("window.start").alias("timestamp"))
                .drop("window", "count");
    }
}
