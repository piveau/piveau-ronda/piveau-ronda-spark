package io.piveau.DataStore.Spark.applications.streaming.normalizer.testApi;

import io.piveau.DataStore.Commons.schema.KafkaKeys;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Commons.schema.harvester.inbound.TestApi.TestApi;
import io.piveau.DataStore.Spark.applications.base.BaseApplication;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.testApi.mapper.TestApiMapper;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.urbanInstitute.UrbanInstituteNormalizer;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;

/**
 * The TestApiNormalizer modifies data from the INBOUND_TESTAPI_KEY specified in the commons module.
 * The exact modification is defined in the TestApiMapper class
 */
public class TestApiNormalizer extends BaseApplication {
    public static Logger LOGGER = Logger.getLogger(TestApiNormalizer.class);
    @Override
    public void start(SparkSession spark) {
        Dataset<TestApi> dataset = getDatasetFromKafka(spark,
                KafkaTopics.INBOUND_TESTAPI, KafkaKeys.INBOUND_TESTAPI_KEY);


            dataset.withWatermark("timestamp", "1 hour");
            Dataset<TestApi> anonymizedDataset = TestApiMapper.execute(dataset);

            writeDatasetToKafka(anonymizedDataset, KafkaTopics.OPENDATA_TEST,
                    KafkaKeys.INBOUND_TESTAPI_KEY, "TestapiToOpenData");
    }
}
