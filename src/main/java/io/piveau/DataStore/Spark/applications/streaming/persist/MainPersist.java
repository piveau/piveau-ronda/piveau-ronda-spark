package io.piveau.DataStore.Spark.applications.streaming.persist;

import io.piveau.DataStore.Spark.applications.base.BaseApplication;
import org.apache.log4j.Logger;
import org.apache.spark.sql.SparkSession;

/**
 * The MainPersister start all Persister applications registered in its start mehtod.
 */
public class MainPersist extends BaseApplication {
    public static Logger LOGGER = Logger.getLogger(MainPersist.class);

    public static void main(String[] args) throws Exception {
        BaseApplication app = new MainPersist();
        SparkSession spark = app.createSession(MainPersist.class.getName());
        app.start(spark);
        spark.streams().awaitAnyTermination();
        spark.stop();
    }

    @Override
    public void start(SparkSession spark) {
        LOGGER.info("Starting MainPerister");
        new StreamsToOpenDataApp().start(spark);
    }
}
