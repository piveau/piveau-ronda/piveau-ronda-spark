package io.piveau.DataStore.Spark.applications;

import io.piveau.DataStore.Spark.applications.base.BaseApplication;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.MainNormalizer;
import io.piveau.DataStore.Spark.applications.streaming.persist.MainPersist;
import org.apache.log4j.Logger;
import org.apache.spark.sql.SparkSession;

/**
 * The MainApplication creates a session and starts Sub Applications like the MainNormalizer and MainPersister
 */
public class MainApplication extends BaseApplication {
    public static Logger LOGGER = Logger.getLogger(MainApplication.class);

    public static void main(String[] args) throws Exception {
        BaseApplication app = new MainApplication();
        SparkSession spark = app.createSession(MainApplication.class.getName());
        // Start all Application with spark Session
        app.start(spark);

        // Wait for all Streams to finish (in Error case)
        spark.streams().awaitAnyTermination();
        spark.stop();
    }

    @Override
    public void start(SparkSession spark) {
        // Start normalizer
        LOGGER.info("Starting MainNormalizer");
        new MainNormalizer().start(spark);

        // Start BatchCreator
        LOGGER.info("Starting MainPerister");
        new MainPersist().start(spark);

    }
}
