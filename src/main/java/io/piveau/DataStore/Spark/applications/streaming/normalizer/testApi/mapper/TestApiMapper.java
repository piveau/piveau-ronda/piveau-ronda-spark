package io.piveau.DataStore.Spark.applications.streaming.normalizer.testApi.mapper;

import io.piveau.DataStore.Commons.schema.harvester.inbound.TestApi.TestApi;
import io.piveau.DataStore.Spark.applications.streaming.normalizer.openWeatherMap.mapper.OpenWeatherMapToCityWeather;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;

import java.util.Arrays;
import java.util.Collections;

public class TestApiMapper {
    public static Logger LOGGER = Logger.getLogger(OpenWeatherMapToCityWeather.class);
    static FlatMapFunction<TestApi, TestApi> mapper = testApi -> {
        try {
            TestApi result = transform(testApi);
            return Collections.singletonList(result).iterator();
        } catch (Exception exception) {
            LOGGER.error("Error with transformation");
            return Collections.emptyIterator();
        }
    };

    static TestApi transform(TestApi testApi){
        TestApi result = new TestApi();
        result.setBody(testApi.getBody());
        result.setId(testApi.getId());
        result.setTimestamp(testApi.getTimestamp());
        result.setTitle(testApi.getTitle());
        result.setUserId(testApi.getUserId());

        char[] firstname = testApi.getFirstname().trim().toCharArray();
        for (int i = 0; i < firstname.length; i++) {
            if (i > 0) {
                firstname[i] = '*';
            }
        }
        result.setFirstname(String.valueOf(firstname));

        char[] lastname = testApi.getLastname().trim().toCharArray();
        for (int i = 0; i < lastname.length; i++) {
            if (i > 0) {
                lastname[i] = '*';
            }
        }
        result.setLastname(String.valueOf(lastname));
        return result;
    }

    public static Dataset<TestApi> execute(Dataset<TestApi> dataset) {
        return dataset.flatMap(mapper, Encoders.bean(TestApi.class));
    }
}
