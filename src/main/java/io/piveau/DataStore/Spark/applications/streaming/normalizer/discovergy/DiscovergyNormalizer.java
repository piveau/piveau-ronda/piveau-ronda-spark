package io.piveau.DataStore.Spark.applications.streaming.normalizer.discovergy;

import io.piveau.DataStore.Commons.schema.KafkaKeys;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Commons.schema.harvester.inbound.discovergy.DiscovergyReading;
import io.piveau.DataStore.Commons.schema.harvester.inbound.discovergy.DiscovergyReadingBatch;
import io.piveau.DataStore.Spark.applications.base.BaseApplication;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.Trigger;

public class DiscovergyNormalizer extends BaseApplication {

    @Override
    public void start(SparkSession spark) {
        Encoder<DiscovergyReadingBatch> batchEncoder = Encoders.bean(DiscovergyReadingBatch.class);

        Dataset<DiscovergyReading> readings = getDatasetFromKafka(spark, KafkaTopics.INBOUND_DISCOVERGY,
                KafkaKeys.INBOUND_DISCOVERGY_METER_ALL);

        Dataset<Row> preparedDataset = readings.select(functions.col("timestamp"), functions.struct("*").alias("reading"));

        Dataset<DiscovergyReadingBatch> aggReadings = preparedDataset
                .withWatermark("timestamp", "2 hours")
                .groupBy(functions.window(functions.column("timestamp"), "1 day"))
                .agg(functions.count(functions.lit(1)).alias("count"),
                        functions.collect_list("reading").alias("values")).as(batchEncoder);

        writeDatasetToKafka(aggReadings,KafkaTopics.INBOUND_DISCOVERGY_BATCH,
                KafkaKeys.INBOUND_DISCOVERGY_BATCH,
                "DiscovergyBatchQuery",
                Trigger.ProcessingTime("10 minutes")
                );
    }
}
