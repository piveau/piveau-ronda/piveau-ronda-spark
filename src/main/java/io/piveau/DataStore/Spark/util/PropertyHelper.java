package io.piveau.DataStore.Spark.util;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Utility class to read property file
 * 
 * @author abaghel
 *
 */
public class PropertyHelper {
	private static final Logger logger = Logger.getLogger(PropertyHelper.class);
	private static Properties prop = new Properties();
	private static Properties readPropertyFile(Properties prop, String file) throws IOException {
		InputStream input = PropertyHelper.class.getClassLoader().getResourceAsStream(file);
		try {
			prop.load(input);
		} catch (IOException ex) {
			logger.error(ex);
			throw ex;
		} finally {
			if (input != null) {
				input.close();
			}
		}
		return prop;
	}
	private static void loadProperties(String environment) throws IOException{
		readPropertyFile(prop, "spark-default.properties");
		String file = environment!= null && environment.equals("prod") ? "spark-prod.properties" : "spark-dev.properties";
		readPropertyFile(prop, file);

	}
	public static Properties getProperties() throws IOException{
		if(prop.isEmpty()) {
			loadProperties(System.getenv("env"));
		}
		return prop;
	}

}
